from .decoder import decode


def test_one():
    actual = decode('one.txt')
    expected = """Если бы я не был программистом, 
я бы обязательно стал шофером и уж работал бы не на плюгавенькой легковушке, 
и не на автобусе даже, а на каком-нибудь грузовом чудовище, 
чтобы в кабину надо было забираться по лестнице, 
а колесо чтобы менять с помощью небольшого подъемного крана."""
    assert actual == expected


def test_two():
    actual = decode('two.txt')
    actual_lines  = actual.splitlines(keepends=False)
    assert actual_lines[0] == 'Я был охвачен административным негодованием.'
    assert actual_lines[-2] == 'Даже хуже, чем в обезьяну.'

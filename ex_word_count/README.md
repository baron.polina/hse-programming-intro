# Подсчёт слов

Напишите функции для подсчёта слов в тексте и выбора самых частотных.

Слова разделяйте функций split без параметров. Знаки препинания интерпретируйте как значимые символы (обрезать не нужно).
Никакой нормализации слов делать не нужно!

функия get_word_count должна подсчитывать вхождение слова в текст.

функция get_top должна находить самые частотные слова, и выводить списком, от самого частотного к наименее частотному. 
В случае равного числа вхождений выводите слова с одинаковыми вхождениями в произвольном порядке.
Параметр функции top_size определяет необходимый размер top'а. Если в тексте меньше разных слов чем запрошено, выводите сколько есть.
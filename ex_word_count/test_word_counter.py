from .word_counter import get_word_count, get_top
import sys

sys.setdefaultencoding('utf-8')

with open('anna.txt', 'r', encoding='utf-8') as f:
    ANNA_TEXT = f.read()


def test_word_count():
    assert get_word_count(text='кря  кря кря ', word='кря') == 3
    assert get_word_count(text='кря\tкря\nкря', word='кря') == 3
    assert get_word_count(text='кря\tкря\nкря', word='внезапненько') == 0


def test_word_count_on_anna():
    assert get_word_count(ANNA_TEXT, 'Анна') == 110
    assert get_word_count(ANNA_TEXT, 'Левин') == 156


def test_get_top_small():
    assert get_top(text='кря  кря кря ', top_size=3) == ['кря']
    assert get_top(text='раз два два три три три', top_size=3) == ['три', 'два', 'раз']


def test_get_top_anna():
    expected = ['--', 'и', 'не', 'в', 'что', 'на', 'с', 'он', 'как', 'она', 'я', 'его', 'к', 'ее', 'но', 'это', 'сказал', 'все', 'было', 'о']
    assert get_top(text=ANNA_TEXT, top_size=20) == expected


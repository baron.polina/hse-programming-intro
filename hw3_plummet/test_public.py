import pytest
import pandas as pd
from collections import OrderedDict
import os.path

from .plummet import find_toughest_plummet


SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))


class Case(object):
    def __init__(self, coins, symbol, start_date, end_date, expected):
        self.coins = coins
        self.symbol = symbol
        self.start_date = start_date
        self.end_date = end_date
        self.expected = expected


COINS = pd.read_csv(os.path.join(SCRIPT_DIR, 'coins.csv'))
COINS['datetime'] = pd.to_datetime(COINS['date'])
COINS.set_index('datetime', inplace=True)


TEST_CASES = OrderedDict([
    (
        "test_case_0",
        Case(
            coins=pd.DataFrame(
                data=[
                    ['ADA', 0.029126999999999997, '2017-11-01'],
                    ['ADA', 0.023079, '2017-11-02'],
                    ['ADA', 0.021418, '2017-11-03']
                ],
                columns=['symbol', 'price', 'date'],
                index=pd.to_datetime(['2017-11-01', '2017-11-02', '2017-11-03'])
            ),
            symbol='ADA',
            start_date='2017-11-01',
            end_date='2017-11-03',
            expected={'date': '2017-11-02', 'plummet': -20.764239365537129}
        )
    ),
    (
        "test_case_1",
        Case(
            coins=COINS,
            symbol='ADA',
            start_date='2017-11-01',
            end_date='2017-11-03',
            expected={'date': '2017-11-02', 'plummet': -20.764239365537129}
        )
    ),
    (
        "test_case_2",
        Case(
            coins=COINS,
            symbol='BTC',
            start_date='2013-01-01',
            end_date='2018-06-06',
            expected={'date': '2013-12-19', 'plummet': -23.465054556178131}
        )
    ),
    (
        "test_case_3",
        Case(
            coins=COINS,
            symbol='NEO',
            start_date='2017-10-01',
            end_date='2018-06-01',
            expected={'date': '2018-01-17', 'plummet': -23.003670798531683}
        )
    ),
    (
        "test_case_4",
        Case(
            coins=COINS,
            symbol='GNO',
            start_date='2017-12-10',
            end_date='2018-04-01',
            expected={'date': '2018-01-17', 'plummet': -34.72101942348138}
        )
    ),
    (
        "test_case_5",
        Case(
            coins=COINS,
            symbol='PPT',
            start_date='2017-11-25',
            end_date='2018-06-01',
            expected={'date': '2018-02-06', 'plummet': -21.681034482758609}
        )
    )
])


@pytest.mark.parametrize(
    'test_case',
    TEST_CASES.values(),
    ids=list(TEST_CASES.keys())
)
def test_find_toughest_plummet(test_case):
    info = find_toughest_plummet(
        test_case.coins,
        test_case.symbol,
        test_case.start_date,
        test_case.end_date)
    assert info['date'] == test_case.expected['date']
    assert pytest.approx(info['plummet'], rel=1e-3) == \
        test_case.expected['plummet']

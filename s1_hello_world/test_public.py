from .hello_world import hello_world


def test_hello_world_callable():
    hello_world()


def test_hello_world():
    result_hello_world = hello_world()
    assert isinstance(result_hello_world, str)
    assert result_hello_world.lower() == 'hello world'

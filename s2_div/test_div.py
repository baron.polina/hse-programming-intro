import sys
import io

from .div import divide


def test_divide():
    assert divide(8, 2) == 4
    assert divide(3, 7) == 0
    assert divide(-3, 1) == -3


def test_zero_divisor():
    buffer = io.StringIO()
    sys.stderr = buffer
    with buffer:
        assert divide(8, 0) is None
        assert 'warning' in buffer.getvalue().lower()

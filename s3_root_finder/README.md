# Root finder

Петя прогуливался по главной улице города и захотел присесть на лавочку, 
чтобы поразмышлять о жизни. Однако не всякая лавочка подойдет Пете 
для раздумий. Если вокруг будет много шума, то Петя будет сбиваться с мысли,
если же будет слишком тихо, то Петя подвергнется депрессивному настроению.

В каждой точке улицы Петя может субъективно оценить уровень шума неким числом `f(x)`, 
где `x`-координата конкретной точки улицы. 
Комфортный для размышлений уровень он обозначет нулем (`f(x) = 0`). 
Места пошумнее описываются положительными числами (`f(x) > 0`), 
слишком тихие отрицательными (`f(x) < 0`).

Петя знает, что в начале улицы (точка `A`) слишком шумно, 
а в конце улицы (точка `B`) слишком тихо. Понятно, что шум для близких точек пространства не сильно отличается. 
Иными словами Петина функция дискомфорта `f`  непрерывна на отрезке `[A, B]`.

Петя просит вас найти такую точку `x_0`, что `f(x_0) = 0`. Для этого предлагается реализовать 
функцию `root_finder(f, A, B)`, которая будет возвращать такое число `x_1`, что `|x_1 - x_0| <= 10 ** (-9)` 
какой-нибудь из точек `x_0`, таких, что `f(x_0) = 0` и `A <= x_1 <= B`.

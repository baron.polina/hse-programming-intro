from typing import Callable


def root_finder(
    func: Callable[[float], float],
    left: float,
    right: float,
    accuracy: float = 10 ** -9
):
    """Finds approximation of the root of function func in interval [left, right]
    assuming than func is continuous and values of function func have different sign in points left and right.

    :param func: Continuous
    :param left: left interval boundary
    :param right: right interval boundary
    :param accuracy: absolute approximation error
    :return:
    """
    raise NotImplementedError

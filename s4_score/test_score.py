import pathlib
from typing import List
from .score import get_ipv6_packages, extract_payload


def extract_all_payloads(path: pathlib.Path) -> List[str]:
    result = []
    for package in get_ipv6_packages(path):
        payload = extract_payload(package)
        result.append(payload)
    return result


def test_get_ipv6_packages():
    actual = get_ipv6_packages('score_dump.bin')
    assert actual[0] == (
            b'`\x00\x00\x00\x002\x11 \xfd\xbcKE\x11V\x00\x00\xf6\\\x89\xff\xfe\x94\xd7\xcd'
            b'*\x02\x06\xb8\x00\x00\x082\x00\x00\x00\x00\x00\x00\x00\x10\x00P\x00d'
            b'\x00*\x00\x0048656c6c6f2073636f72696e672073657276657221'
    )
    assert actual[-1] == (
        b'`\x00\x00\x00\x00\x0c\x11 *\x02\x06\xb8\x00\x00\x082\x00\x00\x00\x00'
        b'\x00\x00\x00\x10\xfd\xbcKE\x11V\x00\x00\xf6\\\x89\xff\xfe\x94\xd7\xcd'
        b'\x00P\x00d\x00\x04\x00\x003539'
    )


def test_extract_all_payloads():
    actual = extract_all_payloads('score_dump.bin')
    expected = [
        b'Hello scoring server!',
        b'Hello you too',
        b'What is Petr score?',
        b'59'
    ]
    assert actual == expected
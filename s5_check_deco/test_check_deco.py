from numbers import Number
import pytest
from .check_deco import takes


def test():
    @takes(int, str)
    def f(a, b):
        print(a, b)

    f(1, 'abcd')
    with pytest.raises(TypeError) as e_info:
        f(1, 2)

def test_2():
    @takes(int, int)
    def f(a, b, c):
        print(a, b, c)

    f(1, 2, 3)
    with pytest.raises(TypeError) as e_info:
        f(1, 'test', 3)

def test_3():
    @takes(int, int)
    def f(a):
        print(a)

    f(1)
    with pytest.raises(TypeError) as e_info:
        f('test')

def test_4():
    @takes(int)
    def some_name(a):
        "Some doc."
        print(a)

    assert some_name.__name__ is not None
    assert some_name.__doc__ == 'Some doc.'


def test_5():
    @takes(Number, str)
    def f(a, b):
        print(a, b)

    f(1, 'abcd')
